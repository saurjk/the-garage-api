<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
    class Api extends CI_Controller {
       
        function __construct(){
            parent::__construct();
            $this->load->helper(array('url', 'json'));
            $this->load->library('encrypt');
			$this->load->database();
        }
        
        public function index(){
            echo "404 error!";
            //$this->load->view('main/access_denied_view');
        }
		
		public function find_total_used_spots(){
			//$car_spots_used_query = $this->db->query('SELECT id, vehicle_identifier, parking_spot_id FROM tbl_used_spots WHERE active = 1 AND ');
		}
		
		//Method for assigning a parking spot to a vehicle if there are empty spots
		public function assign_parking_spot(){
			$vehicle_identifier = $this->input->post('vehicle_identifier');
			$vehicle_type = $this->input->post('vehicle_type');
			$unused_spots_count = $this->unused_spots_count_by_vehicle_type($vehicle_type);
			$vehicle_type_id = $this->vehicle_type_id($vehicle_type);
			
			$response = array();
			if($this->is_vehicle_already_inside($vehicle_identifier,$vehicle_type_id )){
					$response["result"] = 2; //Vehicle Already Inside
			}else{
				if($unused_spots_count == 0){
					$response["result"] = 0; //No Unused Spots
				}else{
				
					$randomly_selected_spot = $this->unused_spot($vehicle_type);
					
					$query_floor = $this->db->get_where('tbl_floor', array('id' => $randomly_selected_spot->floor_id));
					$response["floorname"] = $query_floor->row()->floor;
					
					$assign_spot_data = array(
						'vehicle_identifier' => $vehicle_identifier,
						'parking_spot_id' => $randomly_selected_spot->id,
						'vehicle_type_id' => $vehicle_type_id,
						'active' => 1
					);
					$this->db->insert('tbl_used_spots', $assign_spot_data);
					
					$parking_spot_update_date = array(
						'used' => 1
					);
					$this->db->where('id',$randomly_selected_spot->id);
					$this->db->update('tbl_parking_spot_name', $parking_spot_update_date);
					
					$response["assigned_spot"] = $randomly_selected_spot->spot_name;
					$response["result"] = 1;
				}
			}
			echo json_encode($response);
		}
		
		//Method to call when the vehicle exits the garage
		public function exit_garage(){
			$vehicle_identifier = $this->input->post('vehicle_identifier');
			$parking_spot = $this->input->post('parking_spot');
			$parking_spot_id = $this->get_parking_spot_id_by_name($parking_spot);
			
			$exit_data = array(
				'active' => 0
			);
			$this->db->where('vehicle_identifier', $vehicle_identifier);
			$this->db->where('parking_spot_id', $parking_spot_id);
			$this->db->where('active', 1);
			$this->db->update('tbl_used_spots', $exit_data);
			
			$exit_data_spot = array(
				'used' => 0
			);
			$this->db->where('id', $parking_spot_id);
			$this->db->update('tbl_parking_spot_name', $exit_data_spot);
			
			$response['result'] = "1";
			echo json_encode($response);
		}
		
		//Method to get parking spot details by vehicle identifier 
		public function query_garage_detail_by_vehicle_identifier(){
			$vehicle_identifier = $this->input->post('vehicle_identifier');
			
			$sql = "SELECT tus.vehicle_identifier AS vehicle_identifier, tpsn.spot_name AS spot_name, tf.floor AS floor, tvt.vehicle_type AS vehicle_type FROM tbl_used_spots tus LEFT OUTER JOIN tbl_parking_spot_name tpsn ON tus.parking_spot_id = tpsn.id LEFT OUTER JOIN tbl_floor tf ON tpsn.floor_id = tf.id LEFT OUTER JOIN tbl_vehicle_types tvt ON tus.vehicle_type_id = tvt.id WHERE tus.vehicle_identifier = '".$vehicle_identifier."' AND tus.active = '1' ";
			
			$query = $this->db->query($sql);
			
			$response = array();
			
			if($query->num_rows()>0){
				$response['vehicle_identifier'] = $query->row()->vehicle_identifier;
				$response['spot_name'] = $query->row()->spot_name;
				$response['floor'] = $query->row()->floor;
				$response['vehicle_type'] = $query->row()->vehicle_type;
				
				$response['result'] = 1;
			}else{
				$response['result'] = 0;
			}
			echo json_encode($response);
		}
		
		//Method to get parking spot id by name
		private function get_parking_spot_id_by_name($parking_spot){
			$query = $this->db->get_where('tbl_parking_spot_name', array('spot_name'=>$parking_spot));
			return $query->row()->id;
		}
		
		//Method to check if a vehicle is already parked inside or not
		private function is_vehicle_already_inside($vehicle_identifier, $vehicle_type_id){
			$query = $this->db->query('SELECT * FROM tbl_used_spots WHERE vehicle_identifier = "'.$vehicle_identifier.'" AND active = 1 AND vehicle_type_id = "'.$vehicle_type_id.'"');
			if($query->num_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		
		//Method to get a list of unused parking spots
		private function unused_spot($vehicle_type){
			$unused_spots = $this->unused_spots_by_vehicle_type($vehicle_type);
			$randomval = rand(0,$unused_spots->num_rows());
			return $unused_spots->row($randomval);
		}
		
		//Method to get a list of unused parking spots by vehicle type
		private function unused_spots_by_vehicle_type($vehicle_type){
			$vehicle_type_id = $this->vehicle_type_id($vehicle_type);
			$unused_spots_query = "SELECT * FROM tbl_parking_spot_name WHERE status = '1' AND used = '0' AND vehicle_type_id = '".$vehicle_type_id."'";
			$unused_spots = $this->db->query($unused_spots_query); 
			
			return $unused_spots;
		}
		
		//Method to get the number of unused parking spots by vehicle type 
		private function unused_spots_count_by_vehicle_type($vehicle_type){
			$vehicle_type_id = $this->vehicle_type_id($vehicle_type);
			$unused_spots_query = "SELECT * FROM tbl_parking_spot_name WHERE status = '1' AND used = '0' AND vehicle_type_id = '".$vehicle_type_id."'";
			$unused_spots = $this->db->query($unused_spots_query); 
			
			return $unused_spots->num_rows();
		}
		
		//Method to get the number of used parking spots by vehicle type 
		private function used_spots_count_by_vehicle_type($vehicle_type){
			$vehicle_type_id = $this->vehicle_type_id($vehicle_type);
			$unused_spots_query = "SELECT * FROM tbl_parking_spot_name WHERE status = '1' AND used = '1' AND vehicle_type_id = '".$vehicle_type_id."'";
			$unused_spots = $this->db->query($unused_spots_query); 
			
			return $unused_spots->num_rows();
		}
		
		//Method to get a list of free parking spots by vehicle type
		public function get_free_parking_spots(){
			$response = array();
			$response["unused_car_spots"] = $this->unused_spots_count_by_vehicle_type("Car");
			$response["unused_mbike_spots"] = $this->unused_spots_count_by_vehicle_type("Motorbike");
			
			echo json_encode($response);
		}
		
		//Method to get total number of existing floors
		public function get_floors_existing(){
			$query = $this->db->query('SELECT * FROM tbl_floor');
			$total_rows = $query->num_rows();
			
			$postdata["existing_floors"] = $total_rows;
			echo json_encode($postdata);
		}
		
		//User Login
		public function user_login(){
			$username = $this->input->post('username');
			$query = $this->db->query('SELECT id, name, username, password, role, status FROM tbl_user WHERE username = "'.$username.'"');
	
			$postdata = array();
			if($query->num_rows() > 0){	
				$postdata['userdata'] = array();
				$rowuserdata = $query->row();
				
				$userdata = array();
				$userdata['member_username'] = $rowuserdata->username;
				$userdata['member_password'] = $rowuserdata->password;
				$userdata['member_username'] = $rowuserdata->username;
				$userdata['member_name'] = $rowuserdata->name;
				$userdata['tag_role'] = $rowuserdata->role;
				$userdata['tag_status'] = $rowuserdata->status;
				array_push($postdata['userdata'], $userdata);
				
				$postdata['success'] = 1;
			}else{
				$postdata['success'] = 0;
			}

			echo json_encode($postdata);
		}
		
		//Method to  get the id of the vehicle type
		public function vehicle_type_id($vehicle_type){
			$query = $this->db->query('SELECT id FROM tbl_vehicle_types WHERE vehicle_type = "'.$vehicle_type.'"');
			$row = $query->row();
			return $row->id;
		}
		
		//Method to Add a plane of a new floor
		public function add_floor_plan(){	
			$postdata = array();
			
			$floor_value = $this->input->post('floor');
			$car_spots_value = $this->input->post('carspotsvalue');
			$mbike_spots_value = $this->input->post('mbike_spots_val');
			
			$floor_present = $this->is_floor_present($floor_value);
			if($floor_present){
				$postdata['result'] = 2;
			}else{
				
				$this->db->trans_start();
				
				$floor_data = array(
					'floor' => $floor_value,
					'status' => 1
				);
				$this->db->insert('tbl_floor', $floor_data);
				$floor_id = $this->db->insert_id();
				
				$floor_spots_data = array(
					'floor_id' => $floor_id,
					'car_spots' => $car_spots_value,
					'mbike_spots' => $mbike_spots_value
				);
				$this->db->insert('tbl_floor_spots', $floor_spots_data);
				
				if($floor_id < 10) {
					$floorval = '0'.$floor_id;
				}
				
				for($ctr = 0; $ctr < $car_spots_value; $ctr++){
					if(($ctr+1) < 10) {
						$ctrval = '0'.($ctr+1);
					}else{
						$ctrval = $ctr+1;
					}
					$spot_name = $floorval.'CA'.$ctrval;
					$car_data = array(
						'floor_id' => $floor_id,
						'spot_name' => $spot_name,
						'vehicle_type_id' => $this->vehicle_type_id("Car"),
						'status' => 1
					);
					$this->db->insert('tbl_parking_spot_name', $car_data);
				}
				
				for($ctr = 0; $ctr < $mbike_spots_value; $ctr++){
					if(($ctr+1) < 10) {
						$ctrval = '0'.($ctr+1);
					}else{
						$ctrval = $ctr+1;
					}
					$spot_name = $floorval.'MB'.$ctrval;
					$mbike_data = array(
						'floor_id' => $floor_id,
						'spot_name' => $spot_name,
						'vehicle_type_id' => $this->vehicle_type_id("Motorbike"),
						'status' => 1
					);
					$this->db->insert('tbl_parking_spot_name', $mbike_data);
				}
				
				$this->db->trans_complete();
				if($this->db->trans_status() === FALSE){
					$postdata['result'] = 0;
				}else{
					$postdata['result'] = 1;
				}
				
			}
			echo json_encode($postdata);
		}
		
		//Method to check if the floor is already present
		public function is_floor_present($floor_name){
			$query = $this->db->query('SELECT id, floor, status FROM tbl_floor WHERE floor = "'.$floor_name.'"');
			if($query->num_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		
		public function add_user(){
			$name = $this->input->post('name');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			$response = array();
			$userquery = $this->db->get_where('tbl_user', array('username'=>$username));
			if($userquery->num_rows() > 0){
				$response['result'] = false;
				echo json_encode($response);
			}else{
				$user_data = array(
					'name' => $name,
					'username' => $username,
					'password' => $password,
					'role' => 'ADMIN_ROLE',
					'status' => 1
				);
				
				$result = $this->db->insert('tbl_user', $user_data);
				$response['result'] = $result;
				echo json_encode($response);
			}
		}
    }
?>