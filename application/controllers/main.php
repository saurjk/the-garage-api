<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
	class Main extends CI_Controller{
		
		function __construct(){
			parent::__construct();
			$this->load->helper(array('url', 'json'));
			$this->load->library('encrypt');
			
		}
		public function index()	{
			$this->load->view('main/main');
		}
	}

?>